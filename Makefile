.DEFAULT_GOAL=build
init = ./build/.init
app = ./build/gtk-todo
src_app = main.c #$(shell find src/app -type f -name '*.cs') $(version)

clean:
	rm -fr ./build
$(init):
	mkdir -p build
	touch $(init)
init: $(init)

$(app): $(init) $(src_app)
	gcc -g -o $(app) $(src_app) `pkg-config --libs --cflags gtk+-2.0`
build: $(app)
install: $(app)
	cp $(app) /usr/bin/gtk-todo
	cp gtk-todo.desktop /usr/share/applications/gtk-todo.desktop


