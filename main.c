#include <stdlib.h>
#include <gtk/gtk.h>

typedef struct {
	GtkEntry *entry;
	GtkListStore *list;
} Appender;

enum {
	LIST_ITEM = 0,
	N_COLUMNS
};

void addItem(GtkListStore *store, const gchar *str) {
	GtkTreeIter iter;
	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter, LIST_ITEM, str, -1);
}

void removeItem(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data) {
	GtkListStore *list = (GtkListStore*)user_data;
	GtkTreeIter iter;
	gtk_tree_model_get_iter(GTK_TREE_MODEL(list), &iter, path);
	gtk_list_store_remove(list, &iter);
}

void clicked(GtkButton *button, gpointer userData) { Appender *appender = (Appender*)userData;
	const gchar *text = gtk_entry_get_text(appender->entry);

	GtkTreeIter iter;
	gtk_list_store_append(appender->list, &iter);
	gtk_list_store_set(appender->list, &iter, LIST_ITEM, text, -1);

	gtk_entry_set_text(appender->entry, "");
}

static GtkWidget* createWindow(Appender *appender) {

	//create the main window
	GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), "Window");
	gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

	//create the layout
	GtkWidget *mainVbox = gtk_vbox_new(FALSE, 1);
	GtkWidget *inputHBox = gtk_hbox_new(FALSE, 1);
	gtk_container_add(GTK_CONTAINER(window), mainVbox);
	GtkWidget *contentWindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start(GTK_BOX(mainVbox), inputHBox, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(mainVbox), contentWindow, TRUE, TRUE, 0);

	//create the add item row
	GtkWidget *textEntry = gtk_entry_new();
	GtkWidget *addButton = gtk_button_new_with_label("Add");
	gtk_box_pack_start(GTK_BOX(inputHBox), textEntry, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(inputHBox), addButton, FALSE, FALSE, 0);

	//create the list view
	GtkWidget *list = gtk_tree_view_new();
	GtkCellRenderer *renderer = gtk_cell_renderer_text_new ();
	GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", LIST_ITEM, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);
	GtkListStore *listStore = gtk_list_store_new(N_COLUMNS, G_TYPE_STRING);
	gtk_tree_view_set_model(GTK_TREE_VIEW(list), GTK_TREE_MODEL(listStore));
	gtk_container_add((GtkContainer*)contentWindow, list);

	////connect the events
	//connect the enter to the button click
	g_signal_connect_object (textEntry, "activate", G_CALLBACK(gtk_button_clicked), addButton, G_CONNECT_SWAPPED);
	appender->entry = (GtkEntry*)textEntry;
	appender->list = listStore;
	g_signal_connect(addButton, "clicked", G_CALLBACK(clicked), appender); 
	g_signal_connect(list, "row-activated", G_CALLBACK(removeItem), listStore); 

	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL); 
	g_object_unref(listStore);

	return window;
}

int main(int argc, char *argv[]) {

	gtk_init(&argc, &argv);

	Appender *appender = malloc(sizeof(Appender));
	GtkWidget *window = createWindow(appender);
	gtk_widget_show_all(window);
	gtk_main();
	window = NULL;

	free(appender);

	return 0;
}


